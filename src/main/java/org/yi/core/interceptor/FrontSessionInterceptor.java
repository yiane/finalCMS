package org.yi.core.interceptor;

import org.yi.core.common.Constants;
import org.yi.core.utils.StringUtils;
import org.yi.web.account.entity.AccountEntity;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

public class FrontSessionInterceptor implements Interceptor {
	
	@Override
	public void intercept(Invocation i) {
		
		Controller c = i.getController();
		if(c.getSession() == null) {
			c.redirect("/");
		} else {
			// if need filter session
			String requestURI = c.getRequest().getRequestURI();
			if(StringUtils.isInclude(requestURI, Constants.config.getStringArray("front.session.include"))){
				AccountEntity account = (AccountEntity) c.getSession().getAttribute(Constants.FRONT_LOGIN_USER);
				if(account != null){
					i.invoke();
				} else {
					c.redirect("/");
				}
			} else {
				i.invoke();
			}
		}
		
	}

}
