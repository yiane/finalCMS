package org.yi.core.common;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.yi.core.model.FakeModel;

/**
 * 常量表
 */
public final class Constants {
	
	public static PropertiesConfiguration config;
	
	public static FakeModel fake;
	
	public static final String ADMIN_LOGIN_URL = "/admin/account/login";
	
	/**
	 * 校验码码
	 */
	public static final String VERIFY_CODE = "vcode";
	
	/**
	 * session中存储的当前用户key
	 */
	public static final String KEY_LOGIN_ACCOUNT = "current_user";
	
	public static final String FRONT_LOGIN_USER = "front_login_user";
	
	/**
	 * cookie中的区域设置
	 */
	public static final String COOKIE_LOCAL = "_local";
	
	/**
	 * I18N资源
	 */
	public static final String I18N_RES = "_res";
	
	/** 默认每次执行批处理数量 **/
	public static final Integer DEFAULT_BATCH_SIZE = 100;
	
	/**
	 * 目录最大分级
	 */
	public static final Integer MAX_CATEGORY_GRADE = 3;
	
	public static final String BLOCK_ORDER_ASC = "1";
	
	/** 默认分页大小 **/
	public static final Integer DEFAULT_PAGESIZE = 15;
	
}
