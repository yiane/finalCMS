package org.yi.core.enums;

public enum RoleEnum {

	MANAGER("manager", "超级管理员"),
	REGISTER_USER("register_user", "注册用户"),
	GUEST("guest", "访客");
	
	private String code;
	private String desc;
	
	private RoleEnum(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	
	public static RoleEnum parseEnum(String c){
		for(RoleEnum p : values()){
			if(p.getCode() == c) {
				return p;
			}
		}
		return null;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
