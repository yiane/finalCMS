/**
 * 
 */
package org.yi.core.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * Http相关工具方法
 * @author qq
 *
 */
public class HttpUtils {
	
	/**
	 * 获取访问用户的真实ip（包括用户使用多级代理或者反向代理时的真实ip）
	 * @return
	 */
	public String getIpAddr(HttpServletRequest request){
	   
       String ip = request.getHeader("x-forwarded-for");
       if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
           ip = request.getHeader("Proxy-Client-IP");
       }
       if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
           ip = request.getHeader("WL-Proxy-Client-IP");
       }
       if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
           ip = request.getRemoteAddr();
       }
       return ip; 
	}

}
