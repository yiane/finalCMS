package org.yi.core.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;

public class BaseHandler extends Handler {

	@Override
	public void handle(String target, HttpServletRequest request,
			HttpServletResponse response, boolean[] isHandled) {
		request.setAttribute("ctx", getContextPath(request));
		request.setAttribute("url", getBaseURL(request));
		
		nextHandler.handle(target, request, response, isHandled);
	}
	
	//项目根路径
	public String getContextPath(HttpServletRequest request) {
		return request.getContextPath();
	}
	
	//根域名
	public String getBaseURL(HttpServletRequest request) {
		return request.getScheme() + "://"
				+ request.getServerName() + ":" + request.getServerPort()
				+ getContextPath(request);
	}

}
