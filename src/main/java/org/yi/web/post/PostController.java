package org.yi.web.post;

import java.io.File;
import java.util.Calendar;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yi.core.annotation.Action;
import org.yi.core.common.Constants;
import org.yi.core.enums.PostStateEnum;
import org.yi.core.enums.PostTypeEnum;
import org.yi.core.model.Pagination;
import org.yi.core.utils.ParamUtils;
import org.yi.core.utils.RegexUtils;
import org.yi.web.account.entity.AccountEntity;
import org.yi.web.base.BaseController;
import org.yi.web.category.entity.CategoryEntity;
import org.yi.web.post.entity.PostEntity;

import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.Page;

@Action(action="/admin/post")
public class PostController extends BaseController {
	
	private static final Logger logger = LoggerFactory.getLogger(PostController.class);
	
	private static final String FIRST_IMG_PATTERN = "<img src=\"(.+?)\"";
	
	public void index() {
		Pagination pager = getPager();
		Page<PostEntity> page = null;
		try {
			page = PostEntity.dao.getPager(pager, ParamUtils.translateMap(getParaMap()));
			setAttr("page", page);
			setAttr("states", PostStateEnum.values());
			setAttr("stateMap", PostStateEnum.toMap());
			setAttr("categoryList", CategoryEntity.dao.getAll());
			setAttr("categoryMap", CategoryEntity.dao.getIdTitleMap());
			setAttr("authorMap", AccountEntity.dao.getIdTitleMap());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		

		keepPara();
		
		render("list.html");
	}
	
	public void add() {
		setAttr("categoryList", CategoryEntity.dao.getAll());
		setAttr("postTypeList", PostTypeEnum.values());
		render("add.html");
	}
	
	public void save() {
		PostEntity post = getModel(PostEntity.class);
		try {
			if(PostEntity.dao.getByTitle(post.getStr("title")) != null) {
				addError("文章 [" + post.getStr("title") + "] 已存在!");
				keepPara();
				render("add.html");
			} else {
				String content = post.getStr("content");
				String firstImg = RegexUtils.getValue(content, FIRST_IMG_PATTERN);
				// if first image exists, create thumb
				if(StringUtils.isNotBlank(firstImg)) {
					// 文件名
					String firstImgName = firstImg.substring(0, firstImg.indexOf("."));
					// 后缀
					String imgSuffix = firstImg.substring(firstImg.indexOf(".") + 1);
					String firstImgFullPath = PathKit.getWebRootPath() + firstImg;
					if(new File(firstImgFullPath).exists()) {
						Thumbnails.of(firstImgFullPath)
							.size(Constants.config.getInt("thumb.width", 200), Constants.config.getInt("thumb.height", 170))
							.toFile(PathKit.getWebRootPath() + firstImgName + "_thumb." + imgSuffix);
						post.set("thumb", firstImgName + "_thumb." + imgSuffix);
					}
				}
				post.set("author", getCurrentUser().get("id"));
				post.set("create_time", Calendar.getInstance().getTime());
				post.save();
				redirect("/admin/post");
			}
		} catch(Exception e) {
			logger.error(e.getMessage(), e);
			addError("保存文章 [" + post.getStr("title") + "] 出错, " + e.getMessage());
			keepPara();
			render("add.html");
		}
	}
	
	public void edit() {
		String id = getPara("id");
		if(StringUtils.isNotBlank(id)) {
			setAttr("p", PostEntity.dao.findById(id));
		}
		setAttr("categoryList", CategoryEntity.dao.getAll());
		render("add.html");
	}
	
	public void doEdit() {
		PostEntity post = getModel(PostEntity.class);
		try {
			post.set("modify_time", Calendar.getInstance().getTime());
			post.update();
			redirect("/admin/post");
		} catch(Exception e) {
			addError("编辑文章 [" + post.getStr("title") + "] 出错, " + e.getMessage());
			keepPara();
			render("add.html");
		}
	}
	
	public void recylist() {
		try {
			Pagination pager = getPager();
			Page<PostEntity> page = PostEntity.dao.getRecyclePager(pager, this);
			setAttr("page", page);
			setAttr("stateMap", PostStateEnum.toMap());
			setAttr("categoryList", CategoryEntity.dao.getAll());
			setAttr("categoryMap", CategoryEntity.dao.getIdTitleMap());
			setAttr("authorMap", AccountEntity.dao.getIdTitleMap());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		render("recylist.html");
	}
	
	/**
	 * 移到回收站
	 */
	public void recycle(){
		//id存在说明删除单篇文章， 否则说明批量删除
		String id = getPara("id");
		try {
			if(StringUtils.isNotBlank(id)) {
				String[] idArr = id.split(",");
				PostEntity.dao.recycle(idArr);
			}
		} catch (Exception e) {
			addError("移到回收站 [" + id + "] 出错, " + e.getMessage());
			keepPara();
		} finally {
			redirect("/admin/post");
		}
	}
	
	public void delete(){
		//id存在说明删除单篇文章， 否则说明批量删除
		String id = getPara("id");
		try {
			if(StringUtils.isNotBlank(id)) {
				String[] idArr = id.split(",");
				PostEntity.dao.deleteById((Object[])idArr);
			}
		} catch (Exception e) {
			addError("删除 [" + id + "] 出错, " + e.getMessage());
			keepPara();
		} finally {
			redirect("/admin/post/recylist");
		}
	}
	
	public void emptyRecycle() {
		//id存在说明删除单篇文章， 否则说明批量删除
		try {
			PostEntity.dao.deleteAll();
		} catch (Exception e) {
			addError("清空回收站出错, " + e.getMessage());
			keepPara();
		} finally {
			redirect("/admin/post/recylist");
		}
	}
	
	public void repub(){
		//id存在说明删除单篇文章， 否则说明批量删除
		String id = getPara("id");
		try {
			if(StringUtils.isNotBlank(id)) {
				String[] idArr = id.split(",");
				PostEntity.dao.unRecycle(idArr);
			}
		} catch (Exception e) {
			addError("还原 [" + id + "] 出错, " + e.getMessage());
			keepPara();
		} finally {
			redirect("/admin/post/recylist");
		}
	}


}
