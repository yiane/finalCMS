package org.yi.web.security;

import javax.sql.DataSource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.yi.core.common.Constants;
import org.yi.core.enums.AccountStateEnums;
import org.yi.core.exceptions.BaseException;
import org.yi.web.account.entity.AccountEntity;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.DbKit;

public class ShiroRealm extends JdbcRealm {

	/**
	 * authorization
	 */
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		
		String loginname = String.valueOf(principal.iterator().next());
		AccountEntity user = AccountEntity.dao.getByName(loginname);
		
		if(user != null){
			//one user has one role only
			info.addRole(user.getStr("role"));
		}
		
		return info;
	}

	/**
	 * login authentication
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token)  {
		
		UsernamePasswordToken upt = (UsernamePasswordToken) token ;
		
		try {
			String loginName = upt.getUsername().trim();
			String password = String.valueOf(upt.getPassword());
			
			//determine whether an agent exists
			AccountEntity account = AccountEntity.dao.getByName(loginName);
			
			//if exists, determine whether the password is right or not
			if(account != null){
				// if password is blank or password not equal to passwd parameter, it declare passwd parameter wrong
				if(StrKit.isBlank(password) || !password.equals(account.getStr("passwd"))){
					throw new IncorrectCredentialsException("password error!");
				} else if(AccountStateEnums.LOCK.getCode() == account.getInt("state")){
					throw new LockedAccountException(" account [" + loginName + "] locked.");
				} else {
					Session session = SecurityUtils.getSubject().getSession();
					session.setAttribute(Constants.KEY_LOGIN_ACCOUNT, account);
					return new SimpleAuthenticationInfo(loginName, password,  getName());
				}
			}else{
				throw new UnknownAccountException("loginname: " + loginName + " not found!");
			}
		} catch(BaseException e) {
			throw new BaseException("username or password error!");
		}
		
	}

	@Override
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(DbKit.getConfig().getDataSource());
	}

	
	
}
