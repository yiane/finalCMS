package org.yi.web.index;

import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yi.core.annotation.Action;
import org.yi.core.beetl.EncodeURLFunction;
import org.yi.core.common.Constants;
import org.yi.core.enums.BlockTargetEnum;
import org.yi.core.render.YiCaptchaRender;
import org.yi.web.account.entity.AccountEntity;
import org.yi.web.base.BaseController;
import org.yi.web.category.entity.CategoryEntity;
import org.yi.web.sys.entity.SiteEntity;

import com.jfinal.core.Const;

@Action(action = "/")
public class IndexController extends BaseController {
	
	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);
	
	public void index() {
		SiteEntity site = null;
		try {
			site = getSite();
			setAttr("site", site);
			setAttr("categoryList", CategoryEntity.dao.getAll());
			setAttr("categoryMap", CategoryEntity.dao.getIdTitleMap());
			setAttr("authorMap", AccountEntity.dao.getIdTitleMap());
			
			loadBlocks(BlockTargetEnum.INDEX);
			
			setAttr("encodeURL", new EncodeURLFunction(getResponse()));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		render("/themes/"+site.get("theme")+"/index.html");
	}
	
	public void login() {
		forwardAction("/user/login");
	}

	/**
	 * generate verify code
	 */
	public void captcha(){
		YiCaptchaRender captchRender = new YiCaptchaRender();
		render(captchRender);
	}
	
	/**
	 * used for i18n
	 */
	public void setLocal()  {
		String local = getPara(Constants.COOKIE_LOCAL);
		// set default local: CHINA if empty
		local = StringUtils.isBlank(local) ? Locale.CHINA.toString() : local;
		setCookie("Constants.COOKIE_LOCAL", local, Const.DEFAULT_I18N_MAX_AGE_OF_COOKIE);
		renderNull();
	}
}
