package org.yi.web.sys;

import org.apache.commons.lang.StringUtils;
import org.yi.core.annotation.Action;
import org.yi.web.base.BaseController;
import org.yi.web.sys.entity.SiteEntity;

@Action(action = "/admin/site")
public class SiteController extends BaseController{

	public void index() {
		setAttr("site", SiteEntity.dao.getSite());
		render("add.html");
	}
	
	public void save() {
		SiteEntity site = getModel(SiteEntity.class);
		try {
			if(site.get("id") == null || StringUtils.isNotBlank(String.valueOf(site.get("id")))) {
				site.update();
			} else {
				site.save();
			}
			redirect("/admin/site");
		} catch(Exception e) {
			addError("编辑站点信息出错, " + e.getMessage());
			keepPara();
			render("add.html");
		}
	}
	
}
