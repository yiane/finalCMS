package org.yi.web.category;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.yi.core.annotation.Action;
import org.yi.web.base.BaseController;
import org.yi.web.category.entity.CategoryEntity;

@Action(action="/admin/category")
public class CategoryController extends BaseController {
	
	/**
	 * list all categories
	 */
	public void index(){
		List<Map<String, Object>> resultList = CategoryEntity.dao.getAll();
		setAttr("list", resultList);
		render("list.html");
	}
	
	public void add() {
		List<Map<String, Object>> resultList = CategoryEntity.dao.getAll();
		setAttr("list", resultList);
		
		Object pid = getPara("id");
		// if pid exist, get the entity which id equals pid
		if(pid != null && StringUtils.isNotBlank(String.valueOf(pid))) {
			CategoryEntity parent = CategoryEntity.dao.findById(pid);
			setAttr("parent", parent);
		}
		
		render("add.html");
	}
	
	public void save(){
		CategoryEntity cat = getModel(CategoryEntity.class);
		try {
			if(CategoryEntity.dao.getByTitle(cat.getStr("title")) != null) {
				addError("栏目 [" + cat.getStr("title") + "] 已存在!");
				keepPara();
				render("add.html");
			} else {
				if(cat.get("seq") == null)
					cat.set("seq", 0);
				String pid = getPara("parent_cat");
				if(StringUtils.isNotBlank(pid)) {
					CategoryEntity parent = CategoryEntity.dao.findById(pid);
					Object pl = parent.get("level");
					cat.set("level", (pl==null || StringUtils.isBlank((String)pl)) ? pid : pl+"#"+pid);
					cat.set("pid", pid);
				}
				cat.save();
				redirect("/admin/category");
			}
		} catch(Exception e) {
			addError("保存栏目 [" + cat.getStr("title") + "] 出错, " + e.getMessage());
			keepPara();
			render("add.html");
		}
	}
	
	public void edit() {
		String id = getPara("id");
		if(StringUtils.isNotBlank(id)) {
			CategoryEntity entity = CategoryEntity.dao.findById(id);
			setAttr("cat", entity);
			Object pid = entity.get("pid");
			// if pid exist, get the entity which id equals pid
			if(pid != null && StringUtils.isNotBlank(String.valueOf(pid))) {
				CategoryEntity parent = CategoryEntity.dao.findById(pid);
				setAttr("parent", parent);
			}
		}
		List<Map<String, Object>> resultList = CategoryEntity.dao.getAll();
		setAttr("list", resultList);
		render("add.html");
	}
	
	public void doEdit() {
		CategoryEntity cat = getModel(CategoryEntity.class);
		try {
				if(cat.get("seq") == null)
					cat.set("seq", 0);
				String pid = getPara("parent_cat");
				if(StringUtils.isNotBlank(pid)) {
					CategoryEntity parent = CategoryEntity.dao.findById(pid);
					Object pl = parent.get("level");
					cat.set("level", (pl==null || StringUtils.isBlank((String)pl)) ? pid : pl+"#"+pid);
					cat.set("pid", pid);
				}
				cat.update();
				redirect("/admin/category");
		} catch(Exception e) {
			addError("更新栏目 [" + cat.getStr("title") + "] 出错, " + e.getMessage());
			keepPara();
			render("add.html");
		}
	}
	
	public void delete() {
		String id = getPara("id");
		Map<String,Object> result = new HashMap<String,Object>();
		if(StringUtils.isNotBlank(id)) {
			try {
				List<CategoryEntity> subList = CategoryEntity.dao.getSubList(id);
				if(subList == null || subList.size() == 0 ) {
					result.put("result", CategoryEntity.dao.deleteById(id));
				} else {
					result.put("result", false);
					result.put("msg", "必须先清空子栏目才可删除");
				}
			} catch (Exception e) {
				result.put("result", false);
				result.put("msg", e.getMessage());
			}
		}
		renderJson("result", result);
	}
	
	public void updateSeq() {
		String id = getPara("id");
		String s = getPara("s") == null ? "0" : getPara("s");
		Map<String,Object> result = new HashMap<String,Object>();
		if(StringUtils.isNotBlank(id)) {
			try {
				CategoryEntity cat = CategoryEntity.dao.findById(id);
				if(cat != null) {
					cat.set("seq", s);
					cat.update();
					result.put("result",true);
				} else {
					result.put("result", false);
					result.put("msg", "该栏目不存在");
				}
			} catch (Exception e) {
				result.put("result", false);
				result.put("msg", e.getMessage());
			}
		}
		renderJson("result", result);
	}
}
