package org.yi.web.account;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yi.core.annotation.Action;
import org.yi.core.common.Constants;
import org.yi.core.enums.RoleEnum;
import org.yi.web.account.entity.AccountEntity;
import org.yi.web.base.BaseController;

@Action(action = "/user")
public class UserController extends BaseController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	/**
	 * 进入账户管理首页
	 */
	public void index() {
		render("/themes/template/"+getSite().get("theme")+"/index.html");
	}

	public void login() {
		render("/themes/template/"+getSite().get("theme")+"/login.html");
	}
	
	public void ajaxLogin() {
		Map<String,Object> result = new HashMap<String,Object>();
		String email = getPara("email");
		String password = getPara("password");
        try {
        	AccountEntity account = AccountEntity.dao.getByEmail(email);
            if(account == null){
            	result.put("message", "邮箱不存在!");
            	result.put("result", false);
            } else if(!DigestUtils.md5Hex(password +"{" + account.getStr("login_name") + "}").equals(account.get("passwd"))) {
            	result.put("message", "密码错误!");
            	result.put("result", false);
            } else {
            	getSession().setAttribute(Constants.FRONT_LOGIN_USER, account);
				result.put("result", true);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            result.put("result", false);
            result.put("message", "unknow error:" + e.getMessage());
		}
		renderJson(result);
	}
	
	public void regist() {
		render("/themes/"+getSite().get("theme")+"/regist.html");
	}
	
	public void ajaxRegist(){
		Map<String,Object> result = new HashMap<String,Object>();
		String userName = getPara("userName");
		String email = getPara("email");
		String password = getPara("password");
		try {
			if(AccountEntity.dao.getByName(userName) != null) {
				result.put("message", "用户 [" + userName + "] 已存在!");
				result.put("result", false);
			} else if(AccountEntity.dao.getByEmail(email) != null) {
				result.put("message", "邮箱 [" + email + "] 已存在!");
				result.put("result", false);
			} else if(StringUtils.isBlank(password)) {
				result.put("message", "密码不能为空!");
				result.put("result", false);
			} else {
				AccountEntity account = new AccountEntity();
				account.set("login_name", userName);
				account.set("passwd", password);
				account.set("email", email);
				account.set("role", RoleEnum.REGISTER_USER.getCode());
				account.set("passwd", DigestUtils.md5Hex(password +"{" + userName + "}"));
				account.save();
				result.put("message", "注册成功!");
				result.put("result", true);
			}
		} catch (Exception e) {
			logger.error("注册新用户 [" + userName + "] 失败，" + e.getMessage(), e);
			result.put("message", "注册新用户 [" + userName + "] 失败!");
			result.put("result", false);
		}
		renderJson(result);
	}
	
	/**
	 * ajax判断用户是否可注册
	 */
	public void exists() {
		renderJson(AccountEntity.dao.userExist(getPara("key")));
	}
	
	/**
	 * ajax判断用户是否可注册
	 */
	public void emailExists() {
		renderJson(AccountEntity.dao.emailExist(getPara("key")));
	}
	
	public void findPwd() {
		// TODO
	}
	
	public void updatePwd() {
		// TODO
	}
	
	/**
	 * 我的评论页面
	 */
	public void comments() {
		// TODO 我的评论列表
	}
	
	/**
	 *  用户退出登录
	 */
	public void logout(){
		Map<String,Object> obj = new HashMap<String,Object>();
		getSession().invalidate();
		obj.put("result", true);
		renderJson(obj);
	}
}
