package org.yi.web.account;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yi.core.annotation.Action;
import org.yi.core.common.Constants;
import org.yi.core.enums.RoleEnum;
import org.yi.web.account.entity.AccountEntity;
import org.yi.web.base.BaseController;

import com.jfinal.ext.render.CaptchaRender;

@Action(action = "/admin/account")
public class AccountController extends BaseController {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
	
	/**
	 * 进入仪表盘页面
	 */
	public void dashboard() {
		render("/admin/index.html");
	}
	
	/**
	 * 进入账户管理首页
	 */
	public void index() {
		render("/admin/index.html");
	}

	/**
	 * get 请求转到登录页面
	 * 其他请求执行登录操作
	 */
	public void login() {
		Subject currentUser = SecurityUtils.getSubject();
		//if agent has not login, return the index page
		if(currentUser.isAuthenticated()) {
			redirect("/admin/account/dashboard");
		} else {
			render("/admin/account/login.html");
		}
	}
	
	public void doLogin() {
		String vcode = getPara(Constants.VERIFY_CODE);
		//if verify code is blank or not equals verify code in session, return login.html
		if(CaptchaRender.validate(this, vcode.toUpperCase(), Constants.VERIFY_CODE)) {
			addError("verify code error!");
			this.keepPara();
			forwardAction(Constants.ADMIN_LOGIN_URL);
		} else {
			Subject currentUser = SecurityUtils.getSubject();
			//if agent has not login, return the index page
			if(currentUser.isAuthenticated()) {
				redirect("/admin/account/dashboard");
			} else {
				UsernamePasswordToken token = new UsernamePasswordToken(getPara("login_name"), getPara("passwd"));
				if(token != null && getParaToBoolean("remeberme", false)) {
					token.setRememberMe(true);
				}
	            try {
	                currentUser.login(token);
	                redirect("/admin/account/dashboard");
	            } catch (UnknownAccountException e) {
	            	addError("username ["+token.getPrincipal()+"] not found!");
	            	logger.error(e.getMessage(), e);
	            	forwardAction(Constants.ADMIN_LOGIN_URL);
	            } catch (IncorrectCredentialsException e) {
	            	addError("password error!");
	            	logger.error(e.getMessage(), e);
	            	forwardAction(Constants.ADMIN_LOGIN_URL);
	            } catch (LockedAccountException e) {
	            	addError("user ["+token.getPrincipal()+"] locked!");
	            	logger.error(e.getMessage(), e);
	            	forwardAction(Constants.ADMIN_LOGIN_URL);
	            } catch (Exception e) {
	                addError("unknow error: " + e.getMessage());
	                logger.error(e.getMessage(), e);
	                forwardAction(Constants.ADMIN_LOGIN_URL);
	            }
			}
		}
	}
	
	public void regist() {
		render("/admin/account/regist.html");
	}
	
	public void doRegist(){
		AccountEntity account = getModel(AccountEntity.class);
		try {
			if(AccountEntity.dao.getByName(account.getStr("login_name")) != null) {
				addError("用户 [" + account.getStr("login_name") + "] 已存在!");
				keepPara();
				render("regist.html");
			} else if(StringUtils.isBlank(account.getStr("passwd"))) {
				addError("密码不能为空!");
				keepPara();
				render("regist.html");
			} else {
				account.set("role", RoleEnum.MANAGER.getCode());
				account.set("passwd", DigestUtils.md5Hex(account.getStr("passwd") +"{" + account.getStr("login_name") + "}"));
				if(account.save()) {
					redirect(Constants.ADMIN_LOGIN_URL);
				} else {
					
				} 
			}
		} catch (Exception e) {
			addError("create [" + account.getStr("login_name") + "] error!");
			keepPara();
			render("regist.html");
		}
	}
	
	
	
	/**
	 * ajax判断用户是否可注册
	 */
	public void exists() {
		Map<String,Object> obj = new HashMap<String,Object>();
		obj.put("exist", AccountEntity.dao.userExist(getPara("key")));
		renderJson(obj);
	}
	
	/**
	 *  用户退出登录
	 */
	public void logout(){
		SecurityUtils.getSubject().logout();
		getSession().invalidate();
		redirect(Constants.ADMIN_LOGIN_URL);
	}
}
