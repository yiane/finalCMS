//重新加载验证码
function reloadVerifyImg(){
	var $img = $("#verify-img");
	var verifyImgUrl = $img.attr("src");
	$img.attr("src",verifyImgUrl + "?rnd=" + Math.random());
}

function showErr(msg){
	$('#_msg > b').addClass('red').text(msg);
}
function clearErr() {
	$('#_msg > b').removeClass('red').text("");
}
// 全选
function selectAll(ischeck,checkboxs){
	for(var index in checkboxs){
		checkboxs[index].checked = ischeck;
	}
} 

/**注册**/
$('#regBtn').click(function(e){
	var _username = $.trim($('#login_name').val());
	if(_username.length==0) {
		showErr('用户名不能为空');
		return false;
	}/*else{
		$.ajax({
			dataType: "json",
			url: ctx + "/admin/account/exists",
			data: {key:$.trim($('#login_name').val())},
			async: true,
			success: function(r){
				if(r.exist){
					showErr('用户名已存在');
					return false;
				} else {
					clearErr();
				}
			},
			error: function(httpRequest, textStatus, errorThrown) {
				showError(errorThrow);
				return false;
		    }
		});
	}*/
	var _passwd = $.trim($('#passwd').val());
	if(_passwd.length<6 || _passwd.length >32) {
		showErr('密码长度必须为6-32位');
		return false;
	}
	var _repass = $.trim($('#repass').val());
	if(_repass != _passwd) {
		showErr('两次输入密码不一致');
		return false;
	}
	if(!$("#accept").is(':checked')) {
		showErr('请接受协议');
		return false;
	}
	$('#regist').submit();
});