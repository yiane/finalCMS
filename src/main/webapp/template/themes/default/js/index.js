$(document).ready(function(){
	dropdownOpen();//调用
	var _wrap=$('.news-scroll-list');//定义滚动区域
 	var _interval=2000;//定义滚动间隙时间
 	var _moving=0;//需要清除的动画
 	_wrap.hover(function(){
  		clearInterval(_moving);//当鼠标在滚动区域中时，停止滚动
 	},function(){
  		_moving=setInterval(function(){
  			var _field=_wrap.find('li:first');//此变量不可放置于函数起始处，li:first取值是变化的
  			var _h=_field.height();//取得每次滚动高度
  			_field.animate({marginTop:-_h+'px'},600,function(){//通过取负margin值，隐藏第一行
  			_field.css('marginTop',0).appendTo(_wrap);//隐藏后，将该行的margin值置零，并插入到最后，实现无缝滚动
		});
	},_interval);//滚动间隔时间取决于_interval
	}).trigger('mouseleave');//函数载入时，模拟执行mouseleave，即自动滚动
	
	$('[id=stickys-container]').click(function(){  
    	 $(".stickys-container").css('display','block');
		$(".latest-container").css('display','none');
		$("#stickys-container").addClass("active");
		$("#latest-container").removeClass("active");
    });
	$('[id=latest-container]').click(function(){ 
		$(".stickys-container").css('display','none');
		$(".latest-container").css('display','block');
		$("#stickys-container").removeClass("active");
		$("#latest-container").addClass("active");  
    	
    });
	
	/**
	 * 搜素top
	 */
	 $('[id=seachform]').click(function(){
    	var dpy = $(".header-search-slide").css('display');
		if(dpy=='block'){
			$(".header-search-slide").css('display','none');
		}else{
			$(".header-search-slide").css('display','block');
		}
    });
	
	/**
	 * 登录登出 点击事件
	 */
	$('#user-login').click(function(){
		$(".sign").addClass("sign-show");
		$("#sign-in").css('display','block');
		$("#sign-up").css('display','none');
		$("#pwd-in").css('display','none');
		$("#pwd-go").css('display','none');
	});
	$('#user-reg').click(function(){
		$(".sign").addClass("sign-show");
		$("#sign-in").css('display','none');
		$("#sign-up").css('display','block');
		$("#pwd-in").css('display','none');
		$("#pwd-go").css('display','none');
	});
	/**
	 * 登录 - 注册
	 */
	$('.signin-loader').click(function(){
		$("#sign-in").css('display','block');
		$("#sign-up").css('display','none');
		$("#pwd-in").css('display','none');
		$("#pwd-go").css('display','none');
	});
	$('.signup-loader').click(function(){
		$("#sign-in").css('display','none');
		$("#sign-up").css('display','block');
		$("#pwd-in").css('display','none');
		$("#pwd-go").css('display','none');
	});
	
	/**
	 * 关闭
	 */
	 $('.signclose-loader').click(function(){
		$('.sign').removeClass("sign-show");
		$("#pwd-in").css('display','none');
		$("#pwd-go").css('display','none');
	});

	/**
	 * 上升 下降
	 */
	$('#back-to-top').click(function(){
		$('html, body').animate({scrollTop:0}, 'slow');
	});
	$('#back-to-down').click(function(){
		$('html, body').animate({scrollTop: $(document).height()}, 'slow');
	});
	
	$("#main-wrap-left img").lazyload({
	     placeholder : "/themes/default/images/grey.gif",
	     effect      : "fadeIn",
	});
	$("#sidebar img").lazyload({
	     placeholder : "/themes/default/images/grey.gif",
	     effect      : "fadeIn",
	});
	
});

$(window).scroll(function() {  
	var yheight = getScrollTop(); //滚动条距顶端的距离  
	if(yheight >= 600){
		$("#back-to-top").show();
		$("#back-to-down").hide();
	}else{
		$("#back-to-top").hide();
		$("#back-to-down").show();
	}
	var height =document.documentElement.clientHeight;//浏览器可视化窗口的大小  
	var top=parseInt(yheight)+parseInt(height)-217;  
	var divobj=$(".kf");  
	divobj.attr('style','top:'+top+'px;');  
});
function getScrollTop() {  
	var scrollPos = 0;  
	if (window.pageYOffset) {  
	scrollPos = window.pageYOffset; }  
	else if (document.compatMode && document.compatMode != 'BackCompat')  
	{ scrollPos = document.documentElement.scrollTop; }  
	else if (document.body) { scrollPos = document.body.scrollTop; }   
	return scrollPos;   
}

function validateHide(){
	$('.sign-tips').css('height','0px');
}

/**
 * 鼠标划过就展开子菜单，免得需要点击才能展开
 */
function dropdownOpen() {
	var $dropdownLi = $('li.dropdown');
	$dropdownLi.mouseover(function() {
		$(this).addClass('open');
	}).mouseout(function() {
		$(this).removeClass('open');
	});
}

function loginTab(){
	$(".sign").addClass("sign-show");
	$("#sign-in").css('display','block');
	$("#pwd-in").css('display','none');
	$("#pwd-go").css('display','none');
}