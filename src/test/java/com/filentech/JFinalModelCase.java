package com.filentech;

import org.junit.After;
import org.junit.BeforeClass;
import org.yi.web.account.entity.AccountEntity;
import org.yi.web.init.InitConfig;

import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.JFinalConfig;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;

public class JFinalModelCase{
    
    protected static DruidPlugin dp;
    protected static ActiveRecordPlugin activeRecord;
    
    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    	
    	JFinalConfig jc = new InitConfig();
    	dp=new DruidPlugin(jc.getProperty("jdbc.url"), 
    			jc.getProperty("jdbc.username"), jc.getProperty("jdbc.password"), jc.getProperty("jdbc.driverClassName"));
		
		dp.addFilter(new StatFilter());
		
		WallFilter wall = new WallFilter();
		wall.setDbType(jc.getProperty("db.type"));
		dp.addFilter(wall);

		dp.start();
         
        activeRecord = new ActiveRecordPlugin(dp);
        activeRecord.setDialect(new MysqlDialect());
        
        activeRecord.addMapping("f_account", AccountEntity.class);
         
        activeRecord.start();
    }
 
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        activeRecord.stop();
        dp.stop();
    }
 
}
