package com.filentech.core.base;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.yi.core.model.FakeModel;
import org.yi.core.model.FakeBoundModel;
import org.yi.core.utils.FakeUtils;

public class XmlTest {

	@Test
	public void testXml2Model() {
		
	}
	
	@Test
	public void testModel2Xml() {
		
		FakeBoundModel fake = new FakeBoundModel();
		fake.setTitle("文章列表页");
		fake.setReal("^/article/list?cid=(\\d+)&pageNumber=(\\d+)(;jsessionid=.*)?\\$");
		fake.setFake("/article/list_{}_{}.html");
		
		FakeBoundModel fake2 = new FakeBoundModel();
		fake2.setTitle("文章列表页2");
		fake2.setReal("^/article/list?cid=(\\d+)&pageNumber=(\\d+)(;jsessionid=.*)?\\$");
		fake2.setFake("/article/list_{}_{}.html");
		
		FakeModel fakeList = new FakeModel();
		List<FakeBoundModel> list = new ArrayList<FakeBoundModel>();
		list.add(fake);
		list.add(fake2);
		fakeList.setOutBoundList(list);
		
		System.out.println(FakeUtils.convertToXml(fakeList));
		
	}
	
}
