FinalCMS开发记录

版本说明：
v1.0之前版本为涂鸦版，只具备基本功能， 代码、数据库都有待改进，热拔插功能也不会开发，  1.0版本会整体改进

2014.09开始
2014.10.17发布v0.1版
完成内容包括：内容管理、分类管理、友情链接、伪静态
未完成功能： 评论管理、主题管理、网站配置、用户管理、网站静态化、tag管理、结构化数据提交/ping


导入依赖包两种方式：
1、
就如项目根目录/lib下执行以下语句
mvn install:install-file -DgroupId=com.baidu.json -DartifactId=json -Dversion=0.0.1 -Dpackaging=jar -Dfile=json.jar
mvn install:install-file -DgroupId=com.baidu.ueditor -DartifactId=ueditor -Dversion=1.1.2 -Dpackaging=jar -Dfile=ueditor-1.1.2.jar

2、
在eclipse中依次选中launch下的文件， 右键run as执行
3、导入完成后在项目上右键-maven-update project